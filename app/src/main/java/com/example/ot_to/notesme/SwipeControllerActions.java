package com.example.ot_to.notesme;

/**
 * Created by ot_to on 9.2.2018.
 */

public abstract class SwipeControllerActions
{
    public void onLeftClicked(int position){}
    public void onRightClicked(int position){}
}

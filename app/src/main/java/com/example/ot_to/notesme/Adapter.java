package com.example.ot_to.notesme;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ItemViewHolder>
{
    public List<Item> items = new ArrayList<>();

    public class ItemViewHolder extends RecyclerView.ViewHolder
    {
        private ImageView image;
        private TextView text1, text2;

        public ItemViewHolder(View view)
        {
            super(view);
            image = view.findViewById(R.id.image_view);
            text1 = view.findViewById(R.id.textView1);
            text2 = view.findViewById(R.id.textView2);
        }
    }

    public Adapter(List<Item> items)
    {
        this.items = items;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);

        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position)
    {
        Item currentItem = items.get(position);
        holder.image.setImageResource(currentItem.getImage());
        holder.text1.setText(currentItem.getText1());
        holder.text2.setText(currentItem.getText2());
    }

    @Override
    public int getItemCount()
    {
        return items.size();
    }
}

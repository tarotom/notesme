package com.example.ot_to.notesme;

import android.content.Intent;
import android.graphics.Canvas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    private Adapter adapter;
    private SwipeController swipeController = null;
    private List<Item> list = new ArrayList<>();
    private Spinner mySpinner;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setItemDataAdapter();
        setupRecyclerView();

        // spinner
        mySpinner = findViewById(R.id.spinner_nav);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(MainActivity.this, R.layout.custom_spinner_item, getResources().getStringArray(R.array.spinnerOptions));
        spinnerAdapter.setDropDownViewResource(R.layout.custom_spinner_item); // the "visible" item or "default"
        mySpinner.setAdapter(spinnerAdapter);
        mySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id)
            {
                if(position == 1)
                {
                    Toast.makeText(MainActivity.this, "View doesn't exist", Toast.LENGTH_SHORT).show();
                }
                if(position == 2)
                {
                    // Toast.makeText(MainActivity.this, mySpinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, NoteText.class));
                }
                if(position == 3)   // test view
                {
                    // Toast.makeText(MainActivity.this, mySpinner.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, CreateNewItemActivity.class));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){}
        });
    }

    private void setItemDataAdapter()
    {
        list.add(new Item(R.drawable.ic_note, "Note 1", "Description 1"));
        list.add(new Item(R.drawable.ic_event_note, "Note 2", "Description 2"));
        list.add(new Item(R.drawable.ic_library_books, "Note 3", "Description 3"));
        list.add(new Item(R.drawable.ic_library_books, "Note 4", "Description 4"));
        list.add(new Item(R.drawable.ic_note, "Note 5", "Description 1"));
        list.add(new Item(R.drawable.ic_event_note, "Note 6", "Description 2"));
        list.add(new Item(R.drawable.ic_library_books, "Note 7", "Description 3"));
        list.add(new Item(R.drawable.ic_note, "Note 8", "Description 1"));
        list.add(new Item(R.drawable.ic_event_note, "Note 9", "Description 2"));
        list.add(new Item(R.drawable.ic_note, "Note 10", "Description 1"));
        list.add(new Item(R.drawable.ic_library_books, "Note 11", "Description 3"));
        list.add(new Item(R.drawable.ic_note, "Note 12", "Description 1"));
        list.add(new Item(R.drawable.ic_event_note, "Note 13", "Description 2"));
        list.add(new Item(R.drawable.ic_library_books, "Note 14", "Description 3"));
        list.add(new Item(R.drawable.ic_note, "Note 15", "Description 1"));
        list.add(new Item(R.drawable.ic_event_note, "Note 16", "Description 2"));
        list.add(new Item(R.drawable.ic_library_books, "Note 17", "Description 3"));
        list.add(new Item(R.drawable.ic_note, "Note 18", "Description 1"));
        list.add(new Item(R.drawable.ic_event_note, "Note 19", "Description 2"));
        list.add(new Item(R.drawable.ic_event_note, "Note 20", "Description 2"));
        list.add(new Item(R.drawable.ic_library_books, "Note 21", "Description 3"));

        adapter = new Adapter(list);
    }

    private void setupRecyclerView()
    {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        swipeController = new SwipeController(new SwipeControllerActions()
        {
            @Override
            public void onRightClicked(int position)
            {
                adapter.items.remove(position);
                adapter.notifyItemRemoved(position);
                adapter.notifyItemRangeChanged(position, adapter.getItemCount());
            }
        });

        ItemTouchHelper itemTouchhelper = new ItemTouchHelper(swipeController);
        itemTouchhelper.attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new RecyclerView.ItemDecoration()
        {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state)
            {
                swipeController.onDraw(c);
            }
        });
    }
}

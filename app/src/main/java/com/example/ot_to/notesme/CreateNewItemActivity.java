package com.example.ot_to.notesme;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CreateNewItemActivity extends AppCompatActivity implements View.OnClickListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_item);

        createButtons();
    }

    public void createButtons()
    {
        Button buttonChecklist = findViewById(R.id.button_checklist);
        buttonChecklist.setOnClickListener(this);
        Button buttonText = findViewById(R.id.button_text);
        buttonText.setOnClickListener(this);
        Button buttonTest = findViewById(R.id.button_test);
        buttonTest.setOnClickListener(this);
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.button_checklist:
                Toast.makeText(getApplicationContext(), "Add new checklist thing", Toast.LENGTH_LONG).show();
                break;
            case R.id.button_text:
                Toast.makeText(getApplicationContext(), "Add new text thing", Toast.LENGTH_LONG).show();
                break;
            case R.id.button_test:
                Toast.makeText(getApplicationContext(), "This is just a test button", Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }

}
